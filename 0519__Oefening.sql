USE Modernways;
ALTER TABLE Liedjes ADD COLUMN Genre VARCHAR(20);
SET SQL_SAFE_UPDATES = 0;
UPDATE Liedjes SET Genre = 'Hard rock' WHERE Artiest = 'Led Zeppelin' OR Artiest = 'Van Halen';
SET SQL_SAFE_UPDATES = 1;